#include <stdlib.h>
#include <stdio.h>

int main()
{
	char *envvar = "SAMPLE_ENV_VAR";
	char *value = getenv(envvar);

	if(!value){
		fprintf(stderr, "The environment variable %s was not found.\n", envvar);
		exit(1);
	}
	else {
		fprintf(stdout, "The value of the environment variable %s is: %s .\n", envvar, value);
		exit(0);
	}
}
