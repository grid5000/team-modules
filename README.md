Sample Team Modules
===================

This repository is used to illustrate how one can use a group storage filesystem (https://www.grid5000.fr/w/Group_Storage) to share a set of Modules on a team level.

# Usage

Here we assume that the team group storage filesystem is `/srv/storage/factotum@storage1.nancy/`.

First we log into the Nancy frontend and go inside the target filesystem:
```
me@laptop% ssh nancy.g5k
me@fnancy:~$ cd /srv/storage/factotum@storage1.nancy/
```
Then we clone this repository inside the current directory:

```
me@fnancy:/srv/storage/factotum@storage1.nancy$ git clone https://gitlab.inria.fr/grid5000/team-modules
```

From now on, in order to use the "sample" module which is delivered into this repository, along with rest of the platform modules, we just have to prepend its modulespath to the common module path:

```
me@fnancy:/srv/storage/factotum@storage1.nancy$ export MODULEPATH="/srv/storage/factotum@storage1.nancy/team-modules/modules:$MODULEPATH"
```

We first load a commonly used module (see https://www.grid5000.fr/w/Environment_modules) to show that we still have an access to the base set of modules:

```
me@fnancy:/srv/storage/factotum@storage1.nancy$ module load cuda
me@fnancy:/srv/storage/factotum@storage1.nancy$ which nvcc
/grid5000/spack/opt/spack/linux-debian11-x86_64/gcc-10.2.0/cuda-11.6.2-q7xu5a2n2vuokod6kfr6rveivylqrqvb/bin/nvcc
```

Then we load and run our custom module using its two different versions:

```
me@fnancy:/srv/storage/factotum@storage1.nancy$ module load sample
me@fnancy:/srv/storage/factotum@storage1.nancy$ sample
The value of the environment variable SAMPLE_ENV_VAR is: sample version 1.1 .
me@fnancy:/srv/storage/factotum@storage1.nancy$ module load sample/1.0

The following have been reloaded with a version change:
  1) sample/1.1 => sample/1.0

me@fnancy:/srv/storage/factotum@storage1.nancy$ sample
The value of the environment variable SAMPLE_ENV_VAR is: sample version 1.0 .
```

Everybody belonging to the group storage which governs the access to the `/srv/storage/factotum@storage1.nancy/` will be able to do the same and that is how one can organize and manage a set of modules at the team level.
